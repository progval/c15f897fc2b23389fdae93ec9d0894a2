# first, install openjdk 8, and run:
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-arm64/  # replace arm64 with your architecture

# protobuf v2.6.1 (newer versions don't work)
cd
git clone https://github.com/protocolbuffers/protobuf
cd protobuf
git checkout v2.6.1
./configure --prefix=$HOME/.local/
make -j 4
make install

# compile apache-wave
cd
git clone https://github.com/apache/incubator-retired-wave.git
cd incubator-retired-wave/
# apply fix-apache-wave.patch (replace /home/wave-sandbox/ with your home dir)
gradle build

# unpack
cd
tar -xf ~/incubator-retired-wave/wave/build/distributions/wave-0.4.2.tbz2
cd wave-0.4.2
# edit config/reference.conf
./bin/wave